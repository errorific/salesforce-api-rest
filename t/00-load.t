#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'Salesforce::API::REST' ) || print "Bail out!\n";
}

diag( "Testing Salesforce::API::REST $Salesforce::API::REST::VERSION, Perl $], $^X" );
