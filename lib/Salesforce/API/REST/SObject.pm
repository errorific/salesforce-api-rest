package Salesforce::API::REST::SObject;

use warnings;
use strict;

use Moo;
use methods;
use Salesforce::API::REST::SObject::Field;

our $VERSION = '0.07';
$VERSION = eval $VERSION;


has '_client' =>
  is => 'ro',
  required => 1,
  init_arg => 'client';

has type =>
  is => 'ro',
  required => 1;

has metadata =>
  is => 'lazy';

method _build_metadata {
  my $res = $self->_client->get(
    '/services/data/v20.0/sobjects/' . $self->type
    . '/'
  );

  return $res->res->json;
}

has describe =>
  is => 'lazy';

method _build_describe {
  my $res = $self->_client->get(
    '/services/data/v20.0/sobjects/' . $self->type
    . '/describe/'
  );

  return $res->res->json;
}

has field_names =>
  is => 'lazy';

method _build_field_names {
  return map {$_->{name}} @{$self->describe->{fields}};
}

has fields =>
  is => 'lazy';

method _build_fields {
  my $metadata = $self->metadata;

  my $fields;

  foreach my $field (@{$metadata->fields}) {
    $fields->{$field->{name}} =
      Salesforce::API::Rest::SObject::Field->new(
        client => $self->_client,
        meta => $field,
      );
  }
  $fields;
}

method field ($name) {
  $self->fields->{$name} or die "No field by name $name";
}
