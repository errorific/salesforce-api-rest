package Salesforce::API::REST::Query;

use strict;
use warnings;

use Moo;
use methods;

our $VERSION = '0.07';
$VERSION = eval $VERSION;

use Mojo::URL;
use Salesforce::API::REST::Client;

has _client =>
  is => 'ro',
  required => 1,
  init_arg => 'client';

has query =>
  is => 'ro',
  isa => func ($query) {
    die "Query is not a string" if (ref $query)
  },
  required => 1;

has _results =>
  is => 'ro',
  writer => '_set__results';

has _iterator =>
  is => 'lazy',
  clearer => '_clear_iterator';

method _build__iterator {
  my $current = 0;
  return func {
    $self->_results->[$current++];
  }
}

has _done =>
  is => 'ro',
  writer => '_set__done',
  default => func {0};

has _next_url =>
  is => 'ro',
  clearer => '_clear__next_url',
  writer => '_set__next_url';

has current =>
  is => 'ro',
  writer => '_set_current',

method BUILD (@args) {
  my $url = Mojo::URL->new(
    '/services/data/v20.0/query/'
  );
  $url->query->param(q => $self->query);

  my $res = $self->_client->get($url);

  unless ($res->success) {
    die "Call failed with error " . ($res->error || 'null');
  }

  my $response = $res->res->json;

  $self->_set__done(
    $response->{done} 
  );

  unless ($self->_done) {
    unless ($response->{nextRecordsUrl}) {
      die "Query not done but no next records url";
    }
    $self->_set__next_url($response->{nextRecordsUrl});
  }

  $self->_set__results($response->{records});  
}

method _get_next_batch {
  if ($self->_done) {
    die "Can't get next batch if done";
  }

  my $res = $self->_client->get(
    $self->_next_url
  );

  unless ($res->success) {
    die "Call failed with error " . ($res->error || 'null');
  }

  my $response = $res->res->json;

  $self->_set__done(
    $response->{done} 
  );

  if ($self->_done) {
    $self->_clear__next_url;
  } else {
    unless ($response->{nextRecordsUrl}) {
      die "Query not done but no next records url";
    }
    $self->_set__next_url($response->{nextRecordsUrl});
  }

  $self->_set__results($response->{records});

  $self->_clear_iterator;
}

method next {
  my $result = $self->_iterator->();
  unless ($result or $self->_done) {
    $self->_get_next_batch;
    $result = $self->_iterator->();
  }
  return $result;
}
