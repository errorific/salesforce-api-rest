package Salesforce::API::REST::Client;

use strict;
use warnings;

use Moo;
use methods;

our $VERSION = '0.07';
$VERSION = eval $VERSION;

use Mojo::URL;
use Mojo::UserAgent;

has user =>
  is => 'ro',
  required => 1;

has pass =>
  is => 'ro',
  required => 1;

has 'login_uri' =>
  is => 'ro',
  required => 1,
  coerce => func ($uri) {
    if(ref $uri =~ m/Mojo::URL/) {
      return $uri;
    }
    Mojo::URL->new($uri);
  };

has '_client' =>
  is => 'lazy',
  handles => [qw/get patch put post delete/];

method _build__client {
  my $client = Mojo::UserAgent->new;
  $client->inactivity_timeout(60);

  my $login_message = <<SOAP;
<?xml version="1.0" encoding="utf-8" ?>
<env:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
<env:Body>
  <n1:login xmlns:n1="urn:partner.soap.sforce.com">
    <n1:username>$self->{user}</n1:username>
    <n1:password>$self->{pass}</n1:password>
  </n1:login>
</env:Body>
</env:Envelope>
SOAP

  my $res = $client->post(
    $self->login_uri,
    {
      'Content-Type' => 'text/xml',
      'SOAPAction' => 'login',
    },
    $login_message
  );

  my $xml_res = $res->res->body;
  my $sid;
  my $serverurl;

  if ($xml_res =~ m/<sessionId>(.+)<\/sessionId>/) {
    $sid = $1;
  } else {
    die "No session id provided in response";
  }

  if ($xml_res =~ m/<serverUrl>(.+)<\/serverUrl>/) {
    $serverurl = $1;
  } else {
    die "No server url provided in response";
  }

  # Send all requests through the same host
  #$client->http_proxy('https://na4-api.salesforce.com');
  #$client->https_proxy('https://na4-api.salesforce.com');
  $client->http_proxy($serverurl);
  $client->https_proxy($serverurl);
  # Put the OAuth ID into all request headers
  $client->on(start => sub {
    my ($ua, $tx) = @_;
    $tx->req->headers->header('Authorization' => "OAuth $sid");
  });

  return $client;
}
