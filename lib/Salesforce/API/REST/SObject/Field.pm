package Salesforce::API::Rest::SObject::Field;

use warnings;
use strict;

use Moo;
use methods;

our $VERSION = '0.07';
$VERSION = eval $VERSION;

has _client =>
  is => 'ro',
  required => 1,
  init_arg => 'client';

has type =>
  is => 'ro',
  required => 1;

has _meta =>
  is => 'lazy',
  required => 1;

method name {
  $self->_meta->{name}
}

method picklist_values {
  my @names = map {$_->{value}} @{$self->_meta->{picklistValues}};

  return \@names;
}
