package Salesforce::API::REST;

=head1 NAME

Salesforce::API::REST - The great new Salesforce::API::REST!

=head1 VERSION

Version 0.07

=cut

use 5.006;
use strict;
use warnings;

our $VERSION = '0.07';
$VERSION = eval $VERSION;

use Moo;
use methods;

use Salesforce::API::REST::Client;
use Salesforce::API::REST::SObject;
use Salesforce::API::REST::Query;
use Mojo::URL;
use Mojo::JSON;

has user =>
  is => 'ro',
  required => 1;

has pass =>
  is => 'ro',
  required => 1;

has 'login_uri' =>
  is => 'ro',
  default => func {Mojo::URL->new('https://login.salesforce.com/services/Soap/u/24.0')},
  required => 1,
  coerce => func ($uri) {
    if (ref $uri =~ m/Mojo::URL/) {
      return $uri;
    }
    Mojo::URL->new($uri);
  };

has '_client' =>
  is => 'lazy',
  lazy_build => 1;

method _build__client {
  my $client = Salesforce::API::REST::Client->new(
    user => $self->user,
    pass => $self->pass,
    login_uri => $self->login_uri,
  );

  return $client;
}

method sobjects {
  my $res = $self->_client->get(
    '/services/data/v20.0/sobjects/'
  );

#  my $res = $self->_client->GET(
#    '/services/data/v20.0/sobjects/Account/'. $account_id .
#    '/'
#  );

  return $res->res->json;
}

method list_sobjects {
  my $raw_sobjects = $self->sobjects;

  return map {$_->{name}} @{$raw_sobjects->{sobjects}};
}

method search {
  die "not implemented";
}

method query ($query){
  return Salesforce::API::REST::Query->new(
    client => $self->_client,
    query => $query,
  );
}

method recent {
  die "not implemented";
}

method sobject ($type) {
  return Salesforce::API::REST::SObject->new(
    client => $self->_client,
    type => $type,
  );
}

method update ($uri, $data) {
  my $transaction = $self->_client->patch($uri => {
      'Content-Type' => 'application/json'
    } => Mojo::JSON->encode($data)
  );
  unless ($transaction->res->is_status_class(200)) {
    die "Call failed with code " . $transaction->res->code
      . " and body " . $transaction->res->body
      . " and req body " . $transaction->req->body;
  }

  my $response = $transaction->res->json;

  if ($response->{errorCode}) {
    die 'Create failed with error type '
      . $response->{errorCode}
      . ($response->{fields}
          ? ' on fields ' . join (', ', @{$response->{fields}})
          : '')
      . '. Error ' . $response->{message};
  } else {
    return $response->{id};
  }
}

method delete ($uri) {
  my $transaction = $self->_client->delete($uri);
  unless ($transaction->res->is_status_class(204)) {
    die "Call failed with code ". $transaction->res->code;
  }
}

method create ($type, $data) {
  my $transaction = $self->_client->post(
    '/services/data/v20.0/sobjects/' . $type . '/',
    {'Content-Type' => 'application/json'},
    json => $data);

  unless ($transaction->res->is_status_class(201)) {
    die "Call failed with code ". $transaction->res->code;
  }

  my $response = $transaction->res->json;

  if ($response->{errorCode}) {
    die 'Create failed with error type '
      . $response->{errorCode}
      . ($response->{fields}
          ? ' on fields ' . join (', ', @{$response->{fields}})
          : '')
      . '. Error ' . $response->{message};
  } else {
    return $response->{id};
  }
}

__END__

=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use Salesforce::API::REST;

    my $foo = Salesforce::API::REST->new();
    ...


=head1 AUTHOR

Christopher Mckay, C<< <cmckay at iseek.com.au> >>

=head1 ACKNOWLEDGEMENTS

David Meiklejohn for Mojolicious rewrite to resolve the chunked http problems


=head1 LICENSE AND COPYRIGHT

Copyright 2012 Christopher Mckay.

This program is released under the following license: restrictive
